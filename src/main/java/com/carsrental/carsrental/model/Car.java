package com.carsrental.carsrental.model;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@Table(name = "car")
@Data
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotBlank(message = "Podaj Marke Samochodu")
    private String brand;

    @NotBlank(message = "Podaj Model Samochodu")
    private String model;

    @NotBlank(message = "Podaj Typ Samochodu")
    private String type;

    @NotBlank(message = "Wybierz Typ Silnika")
    private String engineType;

    @NotBlank(message = "Podaj Scieżke do zdiecja")
    private String imageUrl;

    private int year;
    private int numberOfDoors;
    private int numberOfSeats;
    private double price;
    private int hours;
    private int capacity;
    private int speedmax;
    private int combustion;
    private int quantity;
    private boolean available;
    private String description;
}
