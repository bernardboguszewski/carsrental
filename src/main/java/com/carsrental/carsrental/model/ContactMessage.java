package com.carsrental.carsrental.model;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@Table(name = "contact_message")
@Data
@NoArgsConstructor
public class ContactMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "email", nullable = false)
    @Email(message = "Podaj adres email")
    @NotBlank(message = "Podaj adres email")
    private String email;

    @NotBlank(message = "Podaj Tytuł")
    private String title;

    @NotBlank(message = "Podaj opis")
    private String desc;
}
