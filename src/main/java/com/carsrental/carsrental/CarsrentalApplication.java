package com.carsrental.carsrental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CarsrentalApplication  {
    public static void main(String[] args) {
        SpringApplication.run(CarsrentalApplication.class, args);
    }
}
