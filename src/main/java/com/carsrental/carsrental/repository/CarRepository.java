package com.carsrental.carsrental.repository;

import com.carsrental.carsrental.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("carRepository")
public interface CarRepository extends JpaRepository<Car, Long> {
    Car findById(int id);
    Car findByModel(String model);
}
