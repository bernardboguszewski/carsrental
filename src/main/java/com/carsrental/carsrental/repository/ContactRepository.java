package com.carsrental.carsrental.repository;

import com.carsrental.carsrental.model.ContactMessage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("contactRepository")
public interface ContactRepository  extends CrudRepository<ContactMessage, Long> {
    ContactMessage findById(int id);
}
