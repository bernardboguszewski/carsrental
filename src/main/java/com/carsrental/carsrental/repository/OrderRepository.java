package com.carsrental.carsrental.repository;

import com.carsrental.carsrental.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findById(int id);
}
