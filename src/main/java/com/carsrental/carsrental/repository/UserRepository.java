package com.carsrental.carsrental.repository;

import com.carsrental.carsrental.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends CrudRepository<User, Long> {
	 User findById(int id);
	 User findByEmail(String email);
	 User findByConfirmationToken(String confirmationToken);
	 User findByEmailAndPassword(String email, String password);
}