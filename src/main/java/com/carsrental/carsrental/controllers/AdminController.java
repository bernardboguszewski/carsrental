package com.carsrental.carsrental.controllers;

import com.carsrental.carsrental.model.Car;
import com.carsrental.carsrental.model.ContactMessage;
import com.carsrental.carsrental.model.Order;
import com.carsrental.carsrental.model.User;
import com.carsrental.carsrental.repository.CarRepository;
import com.carsrental.carsrental.repository.ContactRepository;
import com.carsrental.carsrental.repository.OrderRepository;
import com.carsrental.carsrental.repository.UserRepository;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Controller
public class AdminController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private OrderRepository orderRepository;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView sendMessage(ModelAndView modelAndView){
        List<User> listOfAccount = new ArrayList<>();

        Iterator<User> iterator = userRepository.findAll().iterator();
        while (iterator.hasNext()) {
            listOfAccount.add(iterator.next());
        }

        modelAndView.addObject("listOfAccount", listOfAccount.stream().collect(Collectors.toList()));
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }

    @RequestMapping(value = "/profileInformation", method = RequestMethod.GET)
    public ModelAndView getInformationAboutAccount(@RequestParam("id") String id, ModelAndView modelAndView){

        User user = userRepository.findById(Integer.parseInt(id));

        modelAndView.addObject("accountInfo", user);
        modelAndView.setViewName("admin/profileInformation");
        return modelAndView;
    }

    @RequestMapping(value = "/adminCars", method = RequestMethod.GET)
    public ModelAndView adminCarsPage(ModelAndView modelAndView){
        List<Car> listOfCars = carRepository.findAll();
        modelAndView.addObject("listOfCars", listOfCars.stream().collect(Collectors.toList()));

        modelAndView.setViewName("admin/adminCars");
        return modelAndView;
    }

    @RequestMapping(value = "/carInformation", method = RequestMethod.GET)
    public ModelAndView getInformationAboutCar(@RequestParam("id") String id, ModelAndView modelAndView){

        Car car = carRepository.findById(Integer.parseInt(id));

        modelAndView.addObject("carInfo", car);
        modelAndView.setViewName("admin/carInformation");
        return modelAndView;
    }

    @RequestMapping(value = "/adminMessage", method = RequestMethod.GET)
    public ModelAndView adminMessagePage(ModelAndView modelAndView){
        List<ContactMessage> listOfContactMessage = new ArrayList<>();

        Iterator<ContactMessage> iterator = contactRepository.findAll().iterator();
        while (iterator.hasNext()) {
            listOfContactMessage.add(iterator.next());
        }
        modelAndView.addObject("listOfMessage", listOfContactMessage.stream().collect(Collectors.toList()));
        modelAndView.setViewName("admin/adminMessage");
        return modelAndView;
    }


    @RequestMapping(value = "/messageInformation", method = RequestMethod.GET)
    public ModelAndView getInformationAboutMessage(@RequestParam("id") String id, ModelAndView modelAndView){
        ContactMessage message = contactRepository.findById(Integer.parseInt(id));
        System.out.println(message);

        modelAndView.addObject("messageInfo", message);
        modelAndView.setViewName("admin/messageInformation");
        return modelAndView;
    }

    @RequestMapping(value = "/addCar", method = RequestMethod.GET)
    public ModelAndView addCarInAdminPanel(ModelAndView modelAndView){
        modelAndView.setViewName("admin/addCar");
        return modelAndView;
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public ModelAndView updateAccountProcess(ModelAndView modelAndView, HttpServletRequest request){
        User account = userRepository.findById(Integer.parseInt(request.getParameter("accountId")));

        account.setEmail(request.getParameter("email"));
        account.setFirstName(request.getParameter("firstName"));
        account.setLastName(request.getParameter("lastName"));

        userRepository.save(account);

        modelAndView.addObject("info_messsage", "Pomyślnie zaktualizowano konto użytkownika");
        modelAndView.setViewName("admin/adminMessageInfo");
        return modelAndView;
    }


    @RequestMapping(value = "/updateCar", method = RequestMethod.POST)
    public ModelAndView updateCarProcess(ModelAndView modelAndView, HttpServletRequest request){
        Car car = carRepository.findById(Integer.parseInt(request.getParameter("carID")));

        car.setModel(request.getParameter("model"));
        car.setYear(Integer.parseInt(request.getParameter("year")));
        car.setHours(Integer.parseInt(request.getParameter("hours")));
        car.setSpeedmax(Integer.parseInt(request.getParameter("speedmax")));
        car.setNumberOfDoors(Integer.parseInt(request.getParameter("numberOfDoors")));
        car.setNumberOfSeats(Integer.parseInt(request.getParameter("numberOfSeats")));
        car.setImageUrl(request.getParameter("imageUrl"));
        car.setCombustion(Integer.parseInt(request.getParameter("combustion")));
        car.setPrice(Double.parseDouble(request.getParameter("price")));
        car.setQuantity(Integer.parseInt(request.getParameter("quantity")));

        carRepository.save(car);

        modelAndView.addObject("info_messsage", "Pomyślnie zaktualizowano samochód");
        modelAndView.setViewName("admin/adminMessageInfo");
        return modelAndView;
    }

    @RequestMapping(value = "/replyMessage", method = RequestMethod.POST)
    public ModelAndView replyMessageProces(ModelAndView modelAndView){
        modelAndView.addObject("info_messsage", "Wiadomość została wysłana na podany adres !");
        modelAndView.setViewName("admin/adminMessageInfo");
        return modelAndView;
    }

    @RequestMapping(value = "/adminLogout", method = RequestMethod.GET)
    public ModelAndView logoutAdminUser(ModelAndView modelAndView,HttpServletRequest request, HttpServletResponse respons){
        Cookie cookie = new Cookie("accountID", "");
        cookie.setMaxAge(0);
        respons.addCookie(cookie);
        modelAndView.addObject("info_messsage", "Pomyślnie wylogowano z panelu administratora");
        modelAndView.setViewName("message");
        return modelAndView;
    }

    @RequestMapping(value = "/addNewCar", method = RequestMethod.POST)
    public ModelAndView addCarProcess(ModelAndView modelAndView, HttpServletRequest request){
        Car car = new Car();
        car.setBrand(request.getParameter("brand"));
        car.setModel(request.getParameter("model"));
        car.setEngineType(request.getParameter("engineType"));
        car.setType(request.getParameter("type"));
        car.setYear(Integer.parseInt(request.getParameter("year")));
        car.setHours(Integer.parseInt(request.getParameter("hours")));
        car.setSpeedmax(Integer.parseInt(request.getParameter("speedmax")));
        car.setNumberOfDoors(Integer.parseInt(request.getParameter("numberOfDoors")));
        car.setNumberOfSeats(Integer.parseInt(request.getParameter("numberOfSeats")));
        car.setImageUrl(request.getParameter("imageUrl"));
        car.setCombustion(Integer.parseInt(request.getParameter("combustion")));
        car.setAvailable(Boolean.parseBoolean(request.getParameter("available")));
        car.setPrice(Double.parseDouble(request.getParameter("price")));
        car.setQuantity(Integer.parseInt(request.getParameter("quantity")));
        car.setDescription(request.getParameter("description"));

        carRepository.save(car);
        modelAndView.addObject("info_messsage", "Pomyślnie dodano samochód do bazy danych!");
        modelAndView.setViewName("admin/adminMessageInfo");
        return modelAndView;
    }


    @RequestMapping(value = "/adminRents", method = RequestMethod.GET)
    public ModelAndView adminRentsPage(ModelAndView modelAndView){
        List<Order> allOrders = orderRepository.findAll();
        modelAndView.addObject("allOrders", allOrders.stream().collect(Collectors.toList()));
        modelAndView.setViewName("admin/adminRents");
        return modelAndView;
    }

    @RequestMapping(value = "/orderInformation", method = RequestMethod.GET)
    public ModelAndView getInformationAboutOrder(@RequestParam("id") String id, ModelAndView modelAndView){
        Order order = orderRepository.findById(Integer.parseInt(id));
        User user = userRepository.findById(order.getIdUser());
        Car car = carRepository.findById(order.getIdCar());
        modelAndView.addObject("timeDayOrder",getDifferenceDays(order.getDateFrom(),order.getDateTo()));
        modelAndView.addObject("fullPriceOrder", (car.getPrice()*getDifferenceDays(order.getDateFrom(),order.getDateTo())));
        modelAndView.addObject("order", order);
        modelAndView.addObject("user", user);
        modelAndView.addObject("car", car);
        modelAndView.setViewName("admin/orderInformation");
        return modelAndView;
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

}
