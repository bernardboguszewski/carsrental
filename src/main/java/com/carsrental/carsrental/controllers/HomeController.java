package com.carsrental.carsrental.controllers;
import com.carsrental.carsrental.model.Car;
import com.carsrental.carsrental.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private CarRepository carRepository;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView showHomePage(ModelAndView modelAndView) {
        List<Car> carList = carRepository.findAll();

        modelAndView.addObject("listOfCars", carList);
        System.out.println(carList.size());
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public ModelAndView showContactPage(ModelAndView modelAndView) {
        modelAndView.setViewName("contact");
        return modelAndView;
    }

    @RequestMapping(value = "/about_us", method = RequestMethod.GET)
    public ModelAndView showAboutPage(ModelAndView modelAndView) {
        modelAndView.setViewName("aboutUs");
        return modelAndView;
    }
}
