package com.carsrental.carsrental.controllers;

import com.carsrental.carsrental.model.User;
import com.carsrental.carsrental.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@Controller
public class LoginController {

    private UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLoginPage(ModelAndView modelAndView, User user) {
        modelAndView.addObject("user", user);
        modelAndView.setViewName("login");
        return modelAndView;
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView processLoginForm(ModelAndView modelAndView, @Valid User user, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) throws IOException {

        User userExists = userService.findByEmailAndPassword(user.getEmail(), user.getPassword());

        if (userExists == null) {
            modelAndView.addObject("userNotFound", "Użytkownik o tych danych nie istnieje.");
            modelAndView.setViewName("login");
            modelAndView.addObject("userID", null);
            bindingResult.reject("email");
        }

        Cookie loginCookie = new Cookie("accountID", Integer.toString(userExists.getId()));
        response.addCookie(loginCookie);

        if(userExists.getEmail().equals("admin@admin.admin") && userExists.getPassword().equals("admin") ){
            modelAndView.setViewName("admin/home");
        }else{
            response.sendRedirect(request.getContextPath() + "/index");

        }
        return modelAndView;
    }

    @RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
    public ModelAndView forgotPasswordPage(ModelAndView modelAndView){
        modelAndView.setViewName("forgotPassword");
        return modelAndView;
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public ModelAndView forgotPasswordProccess(ModelAndView modelAndView, @RequestParam("email") String email){

        User userExists = userService.findByEmail(email);

        if (userExists != null) {
            modelAndView.addObject("info_messsage", "Nowe hasło zostało wysłane na poniższy email");
        }else{
            modelAndView.addObject("info_messsage", "Nie ma takiego emaila w naszej bazie danych");
        }
        modelAndView.setViewName("message");
        return modelAndView;
    }

}
