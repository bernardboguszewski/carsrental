package com.carsrental.carsrental.controllers;

import com.carsrental.carsrental.model.Car;
import com.carsrental.carsrental.model.Order;
import com.carsrental.carsrental.model.User;
import com.carsrental.carsrental.repository.CarRepository;
import com.carsrental.carsrental.repository.OrderRepository;
import com.carsrental.carsrental.repository.UserRepository;
import com.carsrental.carsrental.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class RentalCarController {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    private CarService carService;

    @Autowired
    public RentalCarController(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping(value = "/car", method = RequestMethod.GET)
    public ModelAndView showAboutPage(ModelAndView modelAndView, @RequestParam("id") String idCar) {
        Car car = carRepository.findById(Integer.parseInt(idCar));
        modelAndView.addObject("info_car", car);
        modelAndView.setViewName("car");
        return modelAndView;
    }

    @RequestMapping(value = "/car", method = RequestMethod.POST)
    public ModelAndView addCar(ModelAndView modelAndView, @Valid Car car, BindingResult bindingResult, HttpServletRequest request) {

        modelAndView.addObject("confirmationMessage", "Dodano nowy samochód");
        modelAndView.setViewName("index");

        return modelAndView;
    }

    @RequestMapping(value = "/cars", method = RequestMethod.GET)
    public ModelAndView findAllCars(ModelAndView modelAndView) {

        List<Car> allCars = carService.findAllCars();
        modelAndView.addObject("cars", allCars);
        modelAndView.setViewName("cars");

        return modelAndView;
    }

    @RequestMapping(value = "/rentCar", method = RequestMethod.POST)
    public ModelAndView showRentPage(ModelAndView modelAndView, @RequestParam("id") String idCar, @CookieValue("accountID") String idAccount) {
        Car car = carRepository.findById(Integer.parseInt(idCar));
        User user = userRepository.findById(Integer.parseInt(idAccount));
        modelAndView.addObject("info_car", car);
        modelAndView.addObject("info_user", user);
        modelAndView.setViewName("rentcar");
        return modelAndView;
    }

    @RequestMapping(value = "/rentCarProcess", method = RequestMethod.POST)
    public ModelAndView showRentCarProcess(ModelAndView modelAndView, @CookieValue("accountID") String idAccount, @RequestParam("date_from") String dateFrom, @RequestParam("date_to") String dateTo, @RequestParam("carIdInfo") String carIdInfo){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Order order = new Order();
        order.setIdCar(Integer.parseInt(carIdInfo));
        order.setIdUser(Integer.parseInt(idAccount));
        try{
            order.setDateFrom(simpleDateFormat.parse(dateFrom));
            order.setDateTo(simpleDateFormat.parse(dateTo));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        orderRepository.save(order);
        modelAndView.addObject("info_messsage", "Pomyślnie zarezerwowano samochód!");
        modelAndView.setViewName("message");
        return modelAndView;
    }

}
