package com.carsrental.carsrental.controllers;

import com.carsrental.carsrental.model.ContactMessage;
import com.carsrental.carsrental.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContactController {
    @Autowired
    private ContactRepository contactRepository;

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public ModelAndView sendMessage(ModelAndView modelAndView, @RequestParam("title") String title, @RequestParam("email") String email, @RequestParam("description") String desc) {
        ContactMessage message = new ContactMessage();
        message.setTitle(title);
        message.setEmail(email);
        message.setDesc(desc);

        contactRepository.save(message);
        modelAndView.addObject("info_messsage", "Wiadomość została wysłana!");
        modelAndView.setViewName("message");
        return modelAndView;
    }
}
